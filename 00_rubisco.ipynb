{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Rubisco rate laws"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from __future__ import annotations\n",
    "\n",
    "import itertools as it\n",
    "from typing import Callable\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import seaborn as sns\n",
    "from modelbase.ode import Model, Simulator\n",
    "from scipy.optimize import minimize\n",
    "\n",
    "from qtbmodels import get_poolman, get_y0_poolman\n",
    "\n",
    "sns.set_theme(context=\"notebook\", style=\"whitegrid\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def simulate_time_series(\n",
    "    name: str,\n",
    "    model: Model,\n",
    "    concs: dict[str, pd.DataFrame],\n",
    "    fluxes: dict[str, pd.DataFrame],\n",
    ") -> None:\n",
    "    s = Simulator(model)\n",
    "    s.initialise(get_y0_poolman())\n",
    "    s.simulate(time_points=np.linspace(0, 100, 101))\n",
    "    c = s.get_full_results_df()\n",
    "    v = s.get_fluxes_df()\n",
    "    if c is not None and v is not None:\n",
    "        concs[name] = c\n",
    "        fluxes[name] = v\n",
    "\n",
    "\n",
    "def simulate_ss(\n",
    "    name: str,\n",
    "    model: Model,\n",
    "    concs: dict[str, pd.Series],\n",
    "    fluxes: dict[str, pd.Series],\n",
    ") -> None:\n",
    "    s = Simulator(model)\n",
    "    s.initialise(get_y0_poolman())\n",
    "    s.simulate_to_steady_state()\n",
    "    c = s.get_full_results_df()\n",
    "    v = s.get_fluxes_df()\n",
    "    if c is not None and v is not None:\n",
    "        concs[name] = c.iloc[-1]\n",
    "        fluxes[name] = v.iloc[-1]\n",
    "\n",
    "\n",
    "def simulate_phosphate_scan(\n",
    "    name: str,\n",
    "    model: Model,\n",
    "    concs: dict[str, pd.DataFrame],\n",
    "    fluxes: dict[str, pd.DataFrame],\n",
    ") -> None:\n",
    "    s = Simulator(model)\n",
    "    s.initialise(get_y0_poolman())\n",
    "    c, v = s.parameter_scan_with_fluxes(\n",
    "        \"Phosphate_total\",\n",
    "        np.linspace(10, 20, 6),\n",
    "    )\n",
    "    concs[name] = c\n",
    "    fluxes[name] = v\n",
    "\n",
    "\n",
    "def scan_stability(model: Model, e0: float, y0: dict[str, float]) -> dict[float, bool]:\n",
    "    factors: list[float] = [2, 3, 4, 5, 6, 7, 8]\n",
    "    stability = {factor: False for factor in list(reversed(factors)) + factors}\n",
    "    for direction in ([1 / i for i in factors], factors):\n",
    "        for factor in direction:\n",
    "            model.update_parameter(\"E0_rubisco\", e0)\n",
    "            model.scale_parameter(\"E0_rubisco\", factor)\n",
    "            s = Simulator(model).initialise(y0)\n",
    "            s.simulate_to_steady_state()\n",
    "            if (v := s.get_fluxes_df()) is None:\n",
    "                break\n",
    "            # Filter out instable steady state\n",
    "            if v[\"rubisco_co2\"].iloc[-1] < 1e-6:\n",
    "                break\n",
    "            stability[factor] = True\n",
    "    return stability"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Poolman rate law"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def rubisco_0i(\n",
    "    RUBP: float,\n",
    "    vmax: float,\n",
    "    kms_rubp: float,\n",
    ") -> float:\n",
    "    return vmax * RUBP / (RUBP + kms_rubp)\n",
    "\n",
    "\n",
    "def rubisco_1i(\n",
    "    RUBP: float,\n",
    "    vmax: float,\n",
    "    kms_rubp: float,\n",
    "    i1: float,\n",
    "    ki1: float,\n",
    ") -> float:\n",
    "    return vmax * RUBP / (RUBP + kms_rubp * (1 + i1 / ki1))\n",
    "\n",
    "\n",
    "def rubisco_2i(\n",
    "    RUBP: float,\n",
    "    vmax: float,\n",
    "    kms_rubp: float,\n",
    "    i1: float,\n",
    "    ki1: float,\n",
    "    i2: float,\n",
    "    ki2: float,\n",
    ") -> float:\n",
    "    return vmax * RUBP / (RUBP + kms_rubp * (1 + i1 / ki1 + i2 / ki2))\n",
    "\n",
    "\n",
    "def rubisco_3i(\n",
    "    RUBP: float,\n",
    "    vmax: float,\n",
    "    kms_rubp: float,\n",
    "    i1: float,\n",
    "    ki1: float,\n",
    "    i2: float,\n",
    "    ki2: float,\n",
    "    i3: float,\n",
    "    ki3: float,\n",
    ") -> float:\n",
    "    return vmax * RUBP / (RUBP + kms_rubp * (1 + i1 / ki1 + i2 / ki2 + i3 / ki3))\n",
    "\n",
    "\n",
    "def rubisco_4i(\n",
    "    RUBP: float,\n",
    "    vmax: float,\n",
    "    kms_rubp: float,\n",
    "    i1: float,\n",
    "    ki1: float,\n",
    "    i2: float,\n",
    "    ki2: float,\n",
    "    i3: float,\n",
    "    ki3: float,\n",
    "    i4: float,\n",
    "    ki4: float,\n",
    ") -> float:\n",
    "    return (\n",
    "        vmax\n",
    "        * RUBP\n",
    "        / (RUBP + kms_rubp * (1 + i1 / ki1 + i2 / ki2 + i3 / ki3 + i4 / ki4))\n",
    "    )\n",
    "\n",
    "\n",
    "def replace_rubisco_with_poolman_variant(\n",
    "    model: Model, inhibitors: list[tuple[str, str]]\n",
    ") -> Model:\n",
    "    model.remove_reaction(\"rubisco_co2\")\n",
    "\n",
    "    n_inhibitors = len(inhibitors)\n",
    "    args = [\n",
    "        \"RUBP\",\n",
    "        \"Vmax_rubisco_co2\",\n",
    "        \"kms_rubisco_rubp\",\n",
    "    ]\n",
    "    for pair in inhibitors:\n",
    "        args.extend(pair)\n",
    "\n",
    "    model.add_reaction_from_args(\n",
    "        rate_name=\"rubisco_co2\",\n",
    "        function=globals()[f\"rubisco_{n_inhibitors}i\"],\n",
    "        stoichiometry={\n",
    "            \"RUBP\": -1,\n",
    "            \"PGA\": 2,\n",
    "        },\n",
    "        args=args,\n",
    "    )\n",
    "    return model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Time series"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "concs = {}\n",
    "fluxes = {}\n",
    "\n",
    "\n",
    "simulate_time_series(\n",
    "    \"none\",\n",
    "    replace_rubisco_with_poolman_variant(get_poolman(), []),\n",
    "    concs,\n",
    "    fluxes,\n",
    ")\n",
    "simulate_time_series(\n",
    "    \"all\",\n",
    "    get_poolman(),\n",
    "    concs,\n",
    "    fluxes,\n",
    ")\n",
    "simulate_time_series(\n",
    "    \"pga\",\n",
    "    replace_rubisco_with_poolman_variant(get_poolman(), [(\"PGA\", \"Ki_1_1\")]),\n",
    "    concs,\n",
    "    fluxes,\n",
    ")\n",
    "simulate_time_series(\n",
    "    \"fbp\",\n",
    "    replace_rubisco_with_poolman_variant(get_poolman(), [(\"FBP\", \"Ki_1_2\")]),\n",
    "    concs,\n",
    "    fluxes,\n",
    ")\n",
    "simulate_time_series(\n",
    "    \"sbp\",\n",
    "    replace_rubisco_with_poolman_variant(get_poolman(), [(\"SBP\", \"Ki_1_3\")]),\n",
    "    concs,\n",
    "    fluxes,\n",
    ")\n",
    "simulate_time_series(\n",
    "    \"pi\",\n",
    "    replace_rubisco_with_poolman_variant(get_poolman(), [(\"Orthophosphate\", \"Ki_1_4\")]),\n",
    "    concs,\n",
    "    fluxes,\n",
    ")\n",
    "simulate_time_series(\n",
    "    \"nadph\",\n",
    "    replace_rubisco_with_poolman_variant(get_poolman(), [(\"NADPH\", \"Ki_1_5\")]),\n",
    "    concs,\n",
    "    fluxes,\n",
    ")\n",
    "\n",
    "concs = pd.concat(concs)\n",
    "fluxes = pd.concat(fluxes)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, (ax1, ax2) = plt.subplots(1, 2)\n",
    "\n",
    "concs[\"PGA\"].unstack().T.plot(ax=ax1)\n",
    "concs[\"RUBP\"].unstack().T.plot(ax=ax2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Steady-state"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "concs = {}\n",
    "fluxes = {}\n",
    "\n",
    "\n",
    "simulate_ss(\"all\", get_poolman(), concs, fluxes)\n",
    "simulate_ss(\n",
    "    \"pga\",\n",
    "    replace_rubisco_with_poolman_variant(get_poolman(), [(\"PGA\", \"Ki_1_1\")]),\n",
    "    concs,\n",
    "    fluxes,\n",
    ")\n",
    "simulate_ss(\n",
    "    \"fbp\",\n",
    "    replace_rubisco_with_poolman_variant(get_poolman(), [(\"FBP\", \"Ki_1_2\")]),\n",
    "    concs,\n",
    "    fluxes,\n",
    ")\n",
    "simulate_ss(\n",
    "    \"sbp\",\n",
    "    replace_rubisco_with_poolman_variant(get_poolman(), [(\"SBP\", \"Ki_1_3\")]),\n",
    "    concs,\n",
    "    fluxes,\n",
    ")\n",
    "simulate_ss(\n",
    "    \"pi\",\n",
    "    replace_rubisco_with_poolman_variant(get_poolman(), [(\"Orthophosphate\", \"Ki_1_4\")]),\n",
    "    concs,\n",
    "    fluxes,\n",
    ")\n",
    "\n",
    "simulate_ss(\n",
    "    \"nadph\",\n",
    "    replace_rubisco_with_poolman_variant(get_poolman(), [(\"NADPH\", \"Ki_1_5\")]),\n",
    "    concs,\n",
    "    fluxes,\n",
    ")\n",
    "\n",
    "\n",
    "concs = pd.DataFrame(concs).T\n",
    "fluxes = pd.DataFrame(fluxes).T"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# rubisco flux stays incredibly stable\n",
    "fluxes[\"rubisco_co2\"].plot(kind=\"bar\", title=\"SS RuBisCO flux\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fluxes.loc[:, [\"sbpase\", \"fbpase\", \"prk\"]].plot(kind=\"bar\", title=\"SS RuBisCO flux\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "_ = fluxes.loc[:, [\"EX_GAP\", \"EX_starch\"]].plot(kind=\"bar\", title=\"Exporter flux\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "_ = fluxes.loc[:, [\"EX_PGA\", \"EX_DHAP\"]].plot(kind=\"bar\", title=\"Exporter flux\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "concs.loc[:, [\"PGA\", \"RUBP\"]].plot(kind=\"bar\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "concs.loc[:, [\"FBP\", \"SBP\"]].plot(kind=\"bar\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "((fluxes - fluxes.loc[\"all\"]) * 100 / fluxes.loc[\"all\"]).drop(\"all\").abs().max(axis=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Orthophosphate scan"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "concs = {}\n",
    "fluxes = {}\n",
    "\n",
    "\n",
    "simulate_phosphate_scan(\"all\", get_poolman(), concs, fluxes)\n",
    "simulate_phosphate_scan(\n",
    "    \"pga\",\n",
    "    replace_rubisco_with_poolman_variant(get_poolman(), [(\"PGA\", \"Ki_1_1\")]),\n",
    "    concs,\n",
    "    fluxes,\n",
    ")\n",
    "simulate_phosphate_scan(\n",
    "    \"fbp\",\n",
    "    replace_rubisco_with_poolman_variant(get_poolman(), [(\"FBP\", \"Ki_1_2\")]),\n",
    "    concs,\n",
    "    fluxes,\n",
    ")\n",
    "simulate_phosphate_scan(\n",
    "    \"sbp\",\n",
    "    replace_rubisco_with_poolman_variant(get_poolman(), [(\"SBP\", \"Ki_1_3\")]),\n",
    "    concs,\n",
    "    fluxes,\n",
    ")\n",
    "simulate_phosphate_scan(\n",
    "    \"pi\",\n",
    "    replace_rubisco_with_poolman_variant(get_poolman(), [(\"Orthophosphate\", \"Ki_1_4\")]),\n",
    "    concs,\n",
    "    fluxes,\n",
    ")\n",
    "simulate_phosphate_scan(\n",
    "    \"nadph\",\n",
    "    replace_rubisco_with_poolman_variant(get_poolman(), [(\"NADPH\", \"Ki_1_5\")]),\n",
    "    concs,\n",
    "    fluxes,\n",
    ")\n",
    "\n",
    "concs = pd.concat(concs)\n",
    "fluxes = pd.concat(fluxes)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "concs[\"RUBP\"].unstack().T.plot(\n",
    "    title=\"RUBP\",\n",
    "    xlabel=\"Total orthophosphate\",\n",
    "    ylabel=\"Concentration / mM\",\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "concs[\"PGA\"].unstack().T.plot(\n",
    "    title=\"PGA\",\n",
    "    xlabel=\"Total orthophosphate\",\n",
    "    ylabel=\"Concentration / mM\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### All combinations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_all_combinations():\n",
    "    inhibitors = [\n",
    "        (\"pga\", \"PGA\", \"Ki_1_1\"),\n",
    "        (\"fbp\", \"FBP\", \"Ki_1_2\"),\n",
    "        (\"sbp\", \"SBP\", \"Ki_1_3\"),\n",
    "        (\"pi\", \"Orthophosphate\", \"Ki_1_4\"),\n",
    "        (\"nadph\", \"NADPH\", \"Ki_1_5\"),\n",
    "    ]\n",
    "\n",
    "    yield from it.chain(\n",
    "        # ((\"None\", ()),),\n",
    "        it.combinations(inhibitors, 1),\n",
    "        it.combinations(inhibitors, 2),\n",
    "        it.combinations(inhibitors, 3),\n",
    "        it.combinations(inhibitors, 4),\n",
    "    )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "concs = {}\n",
    "fluxes = {}\n",
    "\n",
    "simulate_ss(\"all\", get_poolman(), concs, fluxes)\n",
    "\n",
    "for i in get_all_combinations():\n",
    "    name = \", \".join(x[0] for x in i)\n",
    "    pars = [x[1:] for x in i]\n",
    "    simulate_ss(\n",
    "        name, replace_rubisco_with_poolman_variant(get_poolman(), pars), concs, fluxes\n",
    "    )\n",
    "\n",
    "concs = pd.DataFrame(concs).T\n",
    "fluxes = pd.DataFrame(fluxes).T"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def closeness_to_all(x: pd.DataFrame) -> pd.DataFrame:\n",
    "    return 1 - ((x - x.loc[\"all\"]) / x.loc[\"all\"]).drop(\"all\").abs()\n",
    "\n",
    "\n",
    "closeness_to_all(concs)[\"RUBP\"].sort_values()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "closeness_to_all(concs)[\"PGA\"].sort_values()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Robustness analysis"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = Simulator(get_poolman()).initialise(get_y0_poolman())\n",
    "s.simulate_to_steady_state()\n",
    "ss_conc = s.get_full_results_df().iloc[-1]  # type: ignore\n",
    "ss_flux = s.get_fluxes_df().iloc[-1]  # type: ignore"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def fit_rubisco_vmax(\n",
    "    vmax: float, model: Model, v_rubisco: float, ss_conc: dict[str, float]\n",
    ") -> float:\n",
    "    model.update_parameter(\"E0_rubisco\", vmax)\n",
    "    diff = model.get_fluxes_dict(ss_conc)[\"rubisco_co2\"][0] - v_rubisco\n",
    "    return diff**2\n",
    "\n",
    "\n",
    "\n",
    "default_model = get_poolman()\n",
    "vmaxs = {}\n",
    "stability = {}\n",
    "y0 = get_y0_poolman()\n",
    "\n",
    "for i in get_all_combinations():\n",
    "    name = \", \".join(x[0] for x in i)\n",
    "    pars = [x[1:] for x in i]\n",
    "    model = replace_rubisco_with_poolman_variant(get_poolman(), pars)\n",
    "    if not (\n",
    "        res := minimize(\n",
    "            fit_rubisco_vmax,\n",
    "            x0=default_model.parameters[\"E0_rubisco\"],\n",
    "            args=(\n",
    "                model,\n",
    "                ss_flux[\"rubisco_co2\"],\n",
    "                ss_conc.to_dict(),\n",
    "            ),\n",
    "            method=\"L-BFGS-B\",\n",
    "        )\n",
    "    ).success:\n",
    "        continue\n",
    "\n",
    "    vmax = res.x[0]\n",
    "    vmaxs[name] = vmax\n",
    "    stability[name] = scan_stability(model, vmax, y0)\n",
    "\n",
    "model = replace_rubisco_with_poolman_variant(get_poolman(), [])\n",
    "vmaxs[\"none\"] = minimize(\n",
    "    fit_rubisco_vmax,\n",
    "    x0=default_model.parameters[\"E0_rubisco\"],\n",
    "    args=(\n",
    "        model,\n",
    "        ss_flux[\"rubisco_co2\"],\n",
    "        ss_conc.to_dict(),\n",
    "    ),\n",
    "    method=\"L-BFGS-B\",\n",
    ").x[0]\n",
    "\n",
    "stability[\"none\"] = scan_stability(model, model.parameters[\"E0_rubisco\"], y0)\n",
    "stability[\"all\"] = scan_stability(\n",
    "    default_model, default_model.parameters[\"E0_rubisco\"], y0\n",
    ")\n",
    "\n",
    "pd.DataFrame(stability).T"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Rate law adapted from Witzel 2010"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def one_div(x: float) -> float:\n",
    "    return 1 / x\n",
    "\n",
    "\n",
    "def mul(x: float, y: float) -> float:\n",
    "    return x * y\n",
    "\n",
    "\n",
    "def div(x: float, y: float) -> float:\n",
    "    return x / y\n",
    "\n",
    "\n",
    "def rubisco_witzel_0i(\n",
    "    rubp: float,\n",
    "    s2: float,\n",
    "    vmax: float,\n",
    "    gamma_or_omega: float,\n",
    "    co2: float,\n",
    "    o2: float,\n",
    "    lr: float,\n",
    "    lc: float,\n",
    "    lo: float,\n",
    "    lrc: float,\n",
    "    lro: float,\n",
    ") -> float:\n",
    "    vmax_app = (gamma_or_omega * vmax * s2 / lr) / (1 / lr + co2 / lrc + o2 / lro)\n",
    "    km_app = 1 / (1 / lr + co2 / lrc + o2 / lro)\n",
    "    return (vmax_app * rubp) / (rubp + km_app * (1 + co2 / lc + o2 / lo))\n",
    "\n",
    "\n",
    "def rubisco_witzel_1i(\n",
    "    rubp: float,\n",
    "    s2: float,\n",
    "    vmax: float,\n",
    "    gamma_or_omega: float,\n",
    "    co2: float,\n",
    "    o2: float,\n",
    "    lr: float,\n",
    "    lc: float,\n",
    "    lo: float,\n",
    "    lrc: float,\n",
    "    lro: float,\n",
    "    i1: float,\n",
    "    ki1: float,\n",
    ") -> float:\n",
    "    vmax_app = (gamma_or_omega * vmax * s2 / lr) / (1 / lr + co2 / lrc + o2 / lro)\n",
    "    km_app = 1 / (1 / lr + co2 / lrc + o2 / lro)\n",
    "    return (vmax_app * rubp) / (rubp + km_app * (1 + co2 / lc + o2 / lo + i1 / ki1))\n",
    "\n",
    "\n",
    "def rubisco_witzel_2i(\n",
    "    rubp: float,\n",
    "    s2: float,\n",
    "    vmax: float,\n",
    "    gamma_or_omega: float,\n",
    "    co2: float,\n",
    "    o2: float,\n",
    "    lr: float,\n",
    "    lc: float,\n",
    "    lo: float,\n",
    "    lrc: float,\n",
    "    lro: float,\n",
    "    i1: float,\n",
    "    ki1: float,\n",
    "    i2: float,\n",
    "    ki2: float,\n",
    ") -> float:\n",
    "    vmax_app = (gamma_or_omega * vmax * s2 / lr) / (1 / lr + co2 / lrc + o2 / lro)\n",
    "    km_app = 1 / (1 / lr + co2 / lrc + o2 / lro)\n",
    "    return (vmax_app * rubp) / (\n",
    "        rubp + km_app * (1 + co2 / lc + o2 / lo + i1 / ki1 + i2 / ki2)\n",
    "    )\n",
    "\n",
    "\n",
    "def rubisco_witzel_3i(\n",
    "    rubp: float,\n",
    "    s2: float,\n",
    "    vmax: float,\n",
    "    gamma_or_omega: float,\n",
    "    co2: float,\n",
    "    o2: float,\n",
    "    lr: float,\n",
    "    lc: float,\n",
    "    lo: float,\n",
    "    lrc: float,\n",
    "    lro: float,\n",
    "    i1: float,\n",
    "    ki1: float,\n",
    "    i2: float,\n",
    "    ki2: float,\n",
    "    i3: float,\n",
    "    ki3: float,\n",
    ") -> float:\n",
    "    vmax_app = (gamma_or_omega * vmax * s2 / lr) / (1 / lr + co2 / lrc + o2 / lro)\n",
    "    km_app = 1 / (1 / lr + co2 / lrc + o2 / lro)\n",
    "    return (vmax_app * rubp) / (\n",
    "        rubp + km_app * (1 + co2 / lc + o2 / lo + i1 / ki1 + i2 / ki2 + i3 / ki3)\n",
    "    )\n",
    "\n",
    "\n",
    "def rubisco_witzel_4i(\n",
    "    rubp: float,\n",
    "    s2: float,\n",
    "    vmax: float,\n",
    "    gamma_or_omega: float,\n",
    "    co2: float,\n",
    "    o2: float,\n",
    "    lr: float,\n",
    "    lc: float,\n",
    "    lo: float,\n",
    "    lrc: float,\n",
    "    lro: float,\n",
    "    i1: float,\n",
    "    ki1: float,\n",
    "    i2: float,\n",
    "    ki2: float,\n",
    "    i3: float,\n",
    "    ki3: float,\n",
    "    i4: float,\n",
    "    ki4: float,\n",
    ") -> float:\n",
    "    vmax_app = (gamma_or_omega * vmax * s2 / lr) / (1 / lr + co2 / lrc + o2 / lro)\n",
    "    km_app = 1 / (1 / lr + co2 / lrc + o2 / lro)\n",
    "    return (vmax_app * rubp) / (\n",
    "        rubp\n",
    "        + km_app * (1 + co2 / lc + o2 / lo + i1 / ki1 + i2 / ki2 + i3 / ki3 + i4 / ki4)\n",
    "    )\n",
    "\n",
    "\n",
    "def rubisco_witzel_5i(\n",
    "    rubp: float,\n",
    "    s2: float,\n",
    "    vmax: float,\n",
    "    gamma_or_omega: float,\n",
    "    co2: float,\n",
    "    o2: float,\n",
    "    lr: float,\n",
    "    lc: float,\n",
    "    lo: float,\n",
    "    lrc: float,\n",
    "    lro: float,\n",
    "    i1: float,\n",
    "    ki1: float,\n",
    "    i2: float,\n",
    "    ki2: float,\n",
    "    i3: float,\n",
    "    ki3: float,\n",
    "    i4: float,\n",
    "    ki4: float,\n",
    "    i5: float,\n",
    "    ki5: float,\n",
    ") -> float:\n",
    "    vmax_app = (gamma_or_omega * vmax * s2 / lr) / (1 / lr + co2 / lrc + o2 / lro)\n",
    "    km_app = 1 / (1 / lr + co2 / lrc + o2 / lro)\n",
    "    return (vmax_app * rubp) / (\n",
    "        rubp\n",
    "        + km_app\n",
    "        * (\n",
    "            1\n",
    "            + co2 / lc\n",
    "            + o2 / lo\n",
    "            + i1 / ki1\n",
    "            + i2 / ki2\n",
    "            + i3 / ki3\n",
    "            + i4 / ki4\n",
    "            + i5 / ki5\n",
    "        )\n",
    "    )\n",
    "\n",
    "\n",
    "def replace_rubisco_with_witzel_variant(\n",
    "    model: Model, inhibitors: list[tuple[str, str]]\n",
    ") -> Model:\n",
    "    \"\"\"\n",
    "    gamma = 1 / km_co2\n",
    "    omega = 1 / km_o2\n",
    "    lr = k_er_minus / k_er_plus\n",
    "    lc = k_er_minus / (omega * kcat_carb)\n",
    "    lrc = k_er_minus / (gamma * k_er_plus)\n",
    "    lro = k_er_minus / (omega * k_er_plus)\n",
    "    lo = k_er_minus / (omega * k_oxy)\n",
    "    \"\"\"\n",
    "    model.remove_reaction(\"rubisco_co2\")\n",
    "    model.remove_derived_parameter(\"Vmax_rubisco_co2\")\n",
    "\n",
    "    model.add_parameters(\n",
    "        {\n",
    "            \"O2_mM\": 0.25,\n",
    "            \"CO2_mM\": 0.012,\n",
    "        }\n",
    "    )\n",
    "\n",
    "    model.add_parameters(\n",
    "        {\n",
    "            \"k_er_plus\": 0.15 * 1000,  # 1 / (mM * s)\n",
    "            \"k_er_minus\": 0.0048,  # 1 / s\n",
    "            \"km_co2\": 10.7 / 1000,  # mM\n",
    "            \"km_o2\": 295 / 1000,  # mM\n",
    "            \"rubisco_kcat_carb\": 3.1,\n",
    "            \"rubisco_kcat_oxy\": 1.125,\n",
    "        }\n",
    "    )\n",
    "\n",
    "    model.add_derived_parameter(\n",
    "        \"Vmax_rubisco_co2\", mul, [\"rubisco_kcat_carb\", \"E0_rubisco\"]\n",
    "    )\n",
    "    model.add_derived_parameter(\n",
    "        \"Vmax_rubisco_o2\", mul, [\"rubisco_kcat_oxy\", \"E0_rubisco\"]\n",
    "    )\n",
    "\n",
    "    model.add_derived_parameter(\"gamma\", one_div, [\"km_co2\"])\n",
    "    model.add_derived_parameter(\"omega\", one_div, [\"km_o2\"])\n",
    "    model.add_derived_parameter(\"omega_kcat_carb\", mul, [\"omega\", \"rubisco_kcat_carb\"])\n",
    "    model.add_derived_parameter(\"omega_koxy\", mul, [\"omega\", \"rubisco_kcat_oxy\"])\n",
    "    model.add_derived_parameter(\"omega_ker_plus\", mul, [\"omega\", \"k_er_plus\"])\n",
    "    model.add_derived_parameter(\"gamma_ker_plus\", mul, [\"gamma\", \"k_er_plus\"])\n",
    "    model.add_derived_parameter(\"lr\", div, [\"k_er_minus\", \"k_er_plus\"])\n",
    "    model.add_derived_parameter(\"lc\", div, [\"k_er_minus\", \"omega_kcat_carb\"])\n",
    "    model.add_derived_parameter(\"lrc\", div, [\"k_er_minus\", \"gamma_ker_plus\"])\n",
    "    model.add_derived_parameter(\"lro\", div, [\"k_er_minus\", \"omega_ker_plus\"])\n",
    "    model.add_derived_parameter(\"lo\", div, [\"k_er_minus\", \"omega_koxy\"])\n",
    "\n",
    "    n_inhibitors = len(inhibitors)\n",
    "    args_carb = [\n",
    "        \"RUBP\",\n",
    "        \"CO2_mM\",\n",
    "        \"Vmax_rubisco_co2\",\n",
    "        \"gamma\",  # 1 / km_co2\n",
    "        \"CO2_mM\",\n",
    "        \"O2_mM\",\n",
    "        \"lr\",\n",
    "        \"lc\",\n",
    "        \"lo\",\n",
    "        \"lrc\",\n",
    "        \"lro\",\n",
    "    ]\n",
    "    args_oxy = [\n",
    "        \"RUBP\",\n",
    "        \"O2_mM\",\n",
    "        \"Vmax_rubisco_o2\",\n",
    "        \"omega\",  # 1 / km_o2\n",
    "        \"CO2_mM\",\n",
    "        \"O2_mM\",\n",
    "        \"lr\",\n",
    "        \"lc\",\n",
    "        \"lo\",\n",
    "        \"lrc\",\n",
    "        \"lro\",\n",
    "    ]\n",
    "    for pair in inhibitors:\n",
    "        args_carb.extend(pair)\n",
    "        args_oxy.extend(pair)\n",
    "\n",
    "    model.add_reaction_from_args(\n",
    "        rate_name=\"rubisco_co2\",\n",
    "        function=globals()[f\"rubisco_witzel_{n_inhibitors}i\"],\n",
    "        stoichiometry={\n",
    "            \"RUBP\": -1,\n",
    "            \"PGA\": 2,\n",
    "        },\n",
    "        args=args_carb,\n",
    "    )\n",
    "    model.add_reaction_from_args(\n",
    "        rate_name=\"rubisco_o2\",\n",
    "        function=globals()[f\"rubisco_witzel_{n_inhibitors}i\"],\n",
    "        stoichiometry={\n",
    "            \"RUBP\": -1,\n",
    "            \"ATP\": -0.5,\n",
    "            \"PGA\": 1.5,\n",
    "            # \"PGO\": 1,\n",
    "        },\n",
    "        args=args_oxy,\n",
    "    )\n",
    "    return model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def surface_plot(\n",
    "    ax: plt.Axes,\n",
    "    x,\n",
    "    y,\n",
    "    z,\n",
    "    title: str,\n",
    "    xlabel: str | None = None,\n",
    "    ylabel: str | None = None,\n",
    "    cmap: str = \"viridis\",\n",
    ") -> None:\n",
    "    ax.plot_surface(*np.meshgrid(x, y, indexing=\"ij\"), z, cmap=cmap)  # type: ignore\n",
    "    ax.set_title(title)\n",
    "    if xlabel is not None:\n",
    "        ax.set_xlabel(xlabel)\n",
    "    if ylabel is not None:\n",
    "        ax.set_ylabel(ylabel)\n",
    "\n",
    "\n",
    "m = replace_rubisco_with_witzel_variant(get_poolman(), [])\n",
    "\n",
    "x = np.linspace(0., 0.02, 10)\n",
    "y = np.linspace(0.2, 0.3, 11)\n",
    "vc = np.full((x.shape[0], y.shape[0]), 0.0)\n",
    "vo = np.full((x.shape[0], y.shape[0]), 0.0)\n",
    "\n",
    "for i, co2 in enumerate(x):\n",
    "    for j, o2 in enumerate(y):\n",
    "        m.update_parameters({\"CO2_mM\": co2, \"O2_mM\": o2})\n",
    "        v = m.get_fluxes_df(get_y0_poolman())\n",
    "        vc[i, j] = v[\"rubisco_co2\"].iloc[0]\n",
    "        vo[i, j] = v[\"rubisco_o2\"].iloc[0]\n",
    "\n",
    "\n",
    "fig, (ax1, ax2, ax3) = plt.subplots(\n",
    "    1, 3, subplot_kw={\"projection\": \"3d\"}, figsize=(18, 6)\n",
    ")\n",
    "surface_plot(ax1, x, y, vc, title=\"Carboxylation rate\")\n",
    "surface_plot(ax2, x, y, vo, title=\"Oxygenation rate\")\n",
    "surface_plot(ax3, x, y, (vc / vo), title=\"Vc / Vo\")\n",
    "\n",
    "for ax in (ax1, ax2, ax3):\n",
    "    ax.set_xlabel(\"CO2 / mM\")\n",
    "    ax.set_ylabel(\"O2 / mM\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pd.DataFrame((vc / vo), index=x, columns=y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# maximum 12-13 uM CO2 (aq)\n",
    "# maximum 250 uM O2 (aq) bei 20 % "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def fit_witzel_rubisco_vmax(\n",
    "    vmax: float, model: Model, v_rubisco: float, ss_conc: dict[str, float]\n",
    ") -> float:\n",
    "    model.update_parameter(\"E0_rubisco\", vmax)\n",
    "    v = model.get_fluxes_dict(ss_conc)\n",
    "    diff = (v[\"rubisco_co2\"][0] + v[\"rubisco_o2\"][0]) - v_rubisco\n",
    "    # diff = v[\"rubisco_co2\"][0] - v_rubisco\n",
    "    return diff**2\n",
    "\n",
    "\n",
    "model = replace_rubisco_with_witzel_variant(get_poolman(), [])\n",
    "# model.update_parameter(\"O2_mM\", 0.01)\n",
    "if not (\n",
    "    res := minimize(\n",
    "        fit_witzel_rubisco_vmax,\n",
    "        x0=default_model.parameters[\"E0_rubisco\"],\n",
    "        args=(\n",
    "            model,\n",
    "            ss_flux[\"rubisco_co2\"],\n",
    "            ss_conc.to_dict(),\n",
    "        ),\n",
    "        method=\"L-BFGS-B\",\n",
    "    )\n",
    ").success:\n",
    "    raise ValueError(\"Fuck\")\n",
    "else:\n",
    "    print(\"E0:\", e0 := res.x[0])\n",
    "    model.update_parameter(\"E0_rubisco\", e0 * 0.75)\n",
    "\n",
    "    print(\"Vmax CO2:\", model.parameters[\"Vmax_rubisco_co2\"])\n",
    "    print(\"Vmax O2:\", model.parameters[\"Vmax_rubisco_o2\"])\n",
    "\n",
    "s = Simulator(model).initialise(get_y0_poolman())\n",
    "s.simulate(1000)\n",
    "\n",
    "assert (c := s.get_full_results_df()) is not None\n",
    "assert (v := s.get_fluxes_df()) is not None\n",
    "\n",
    "print(\"Vc/Vo\", (v[\"rubisco_co2\"] / v[\"rubisco_o2\"]).iloc[-1])\n",
    "\n",
    "fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12, 6))\n",
    "s.plot_selection([\"RUBP\", \"PGA\"], ax=ax1)\n",
    "s.plot_flux_selection([\"rubisco_co2\", \"rubisco_o2\"], ax=ax2)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = replace_rubisco_with_witzel_variant(\n",
    "    get_poolman(),\n",
    "    [\n",
    "        (\"PGA\", \"Ki_1_1\"),\n",
    "        (\"FBP\", \"Ki_1_2\"),\n",
    "        (\"SBP\", \"Ki_1_3\"),\n",
    "        (\"Orthophosphate\", \"Ki_1_4\"),\n",
    "        (\"NADPH\", \"Ki_1_5\"),\n",
    "    ],\n",
    ")\n",
    "if not (\n",
    "    res := minimize(\n",
    "        fit_witzel_rubisco_vmax,\n",
    "        x0=default_model.parameters[\"E0_rubisco\"],\n",
    "        args=(\n",
    "            model,\n",
    "            ss_flux[\"rubisco_co2\"],\n",
    "            ss_conc.to_dict(),\n",
    "        ),\n",
    "        method=\"L-BFGS-B\",\n",
    "    )\n",
    ").success:\n",
    "    raise ValueError(\"Fuck\")\n",
    "else:\n",
    "    print(\"E0:\", e0 := res.x[0] * 0.75)\n",
    "    model.update_parameter(\"E0_rubisco\", e0)\n",
    "\n",
    "    print(\"Vmax CO2:\", model.parameters[\"Vmax_rubisco_co2\"])\n",
    "    print(\"Vmax O2:\", model.parameters[\"Vmax_rubisco_o2\"])\n",
    "\n",
    "s = Simulator(model).initialise(get_y0_poolman())\n",
    "s.simulate(1000)\n",
    "\n",
    "assert (c := s.get_full_results_df()) is not None\n",
    "assert (v := s.get_fluxes_df()) is not None\n",
    "\n",
    "# print(v[\"rubisco_co2\"] / v[\"rubisco_o2\"])\n",
    "\n",
    "fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12, 6))\n",
    "s.plot_selection([\"RUBP\", \"PGA\"], ax=ax1)\n",
    "s.plot_flux_selection([\"rubisco_co2\", \"rubisco_o2\"], ax=ax2)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = replace_rubisco_with_witzel_variant(\n",
    "    get_poolman(),\n",
    "    [\n",
    "        (\"PGA\", \"Ki_1_1\"),\n",
    "        (\"FBP\", \"Ki_1_2\"),\n",
    "        (\"SBP\", \"Ki_1_3\"),\n",
    "        (\"Orthophosphate\", \"Ki_1_4\"),\n",
    "        (\"NADPH\", \"Ki_1_5\"),\n",
    "    ],\n",
    ")\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.parameters['CO2_mM'] / model.parameters[\"lc\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.parameters['O2_mM'] / model.parameters['lo']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "get_y0_poolman()[\"PGA\"] / model.parameters[\"Ki_1_1\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "get_y0_poolman()[\"FBP\"] / model.parameters[\"Ki_1_2\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "get_y0_poolman()[\"SBP\"] / model.parameters[\"Ki_1_3\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.get_full_concentration_dict(get_y0_poolman())[\"Orthophosphate\"][0] / model.parameters[\"Ki_1_4\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.parameters[\"NADPH\"] / model.parameters[\"Ki_1_5\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "py311",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
